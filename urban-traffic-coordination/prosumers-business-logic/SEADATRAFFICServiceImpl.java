package eu.chorevolution.prosumer.seadatraffic.business.impl.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.seadatraffic.AreaOfInterestRequest;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsRequest;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsResponse;
import eu.chorevolution.prosumer.seadatraffic.AreaTrafficInformation;
import eu.chorevolution.prosumer.seadatraffic.RouteSegmentPoints;
import eu.chorevolution.prosumer.seadatraffic.RouteSegmentTrafficInfo;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.Waypoint;
import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.seadatraffic.business.SEADATRAFFICService;
import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

@Service
public class SEADATRAFFICServiceImpl implements SEADATRAFFICService {




	@Override
	public void setAreaOfInterest(AreaOfInterestRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of AreaOfInterestRequest message from senderParticipantName
		*/
	}     

	@Override
	public void setSegmentTrafficInformation(SegmentTrafficInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of SegmentTrafficInformationResponse message from senderParticipantName
		*/
	}     

    @Override
	public AreaSegmentsRequest createAreaSegmentsRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    		AreaSegmentsRequest result = new AreaSegmentsRequest();
    	/**
    	 * Create areaSegmentsRequests (origin and destination waypoints) from the sequence of areaName (string type) in areaOfInterestRequest
    	 * This origin and destination will be provided to request for routeSuggestions type of information from DTS-AREA-TRAFFIC provider
    	 * DTS-AREA-TRAFFIC provider will request for routes from routing engines to form the responses
		 */	
    		AreaOfInterestRequest areaOfInterest = (AreaOfInterestRequest) choreographyInstanceMessages.getMessage("areaOfInterestRequest");
	    	// Some list of points here from whatever area names. 
	    	Waypoint waypoint0 = new Waypoint();
	    	Waypoint waypoint1 = new Waypoint();
	    	
	    	// Temporarily set the origin and destination addresses from the default values in the client. Has to be changed later
	    	if (areaOfInterest.getAreaName().equals("Lindholmen2Nordstan")) {
	    		waypoint0.setLat(57.7075314);
	    		waypoint0.setLon(11.940698699999984);
	    		waypoint1.setLat(57.70870910000002);
	    		waypoint1.setLon(11.969142499999975);
	    	}
	    	result.setOrigin(waypoint0);
	    	result.setDestination(waypoint1);
	    	return result;
    }
    
    @Override
	public AreaTrafficInformation createAreaTrafficInformation(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
	    	AreaTrafficInformation result = new AreaTrafficInformation();
	    	/**
	    	 * create AreaTrafficInformation from segmentTrafficInformationResponse for all segments
			 */	
	    	List<SegmentTrafficInformationResponse> allSegmentsResponse = choreographyInstanceMessages.getMessages("segmentTrafficInformationResponse");
	    	for (Iterator<SegmentTrafficInformationResponse> i = allSegmentsResponse.iterator(); i.hasNext();) {
	    		RouteSegmentTrafficInfo segmentTrafficItem = i.next().getSegment();
	    		result.getAreaSegmentsTrafficInfo().add(segmentTrafficItem);
	    	}
	    	return result;
    }
    
    @Override
	public SegmentTrafficInformationRequest createSegmentTrafficInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
	    	SegmentTrafficInformationRequest result = new SegmentTrafficInformationRequest();
	    	/**
	    	 * Create SegmentTrafficInformationRequest upon receiving areaSegmentsResponse loop iterations defined by loopIndex
			 */		  
		AreaSegmentsResponse inMessage = (AreaSegmentsResponse) choreographyInstanceMessages
				.getMessages("areaSegmentsResponse")
				.get(choreographyInstanceMessages.getMessages("areaSegmentsResponse").size() - 1);
		List<RouteSegmentPoints> allSegmentsOfInterest = new ArrayList<RouteSegmentPoints>();
		allSegmentsOfInterest.addAll(inMessage.getAreaSegments());
		int indexValue = choreographyLoopIndexes
				.getChoreographyLoopIndex("TrafficSegmentInformationCollection");
		result.setWaypoint0(allSegmentsOfInterest.get(indexValue).getWaypoint0());
		result.setWaypoint1(allSegmentsOfInterest.get(indexValue).getWaypoint1());
		return result;
    }
    
	@Override    
    public void receiveAreaSegmentsResponse(AreaSegmentsResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (AreaSegmentsResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    

}
