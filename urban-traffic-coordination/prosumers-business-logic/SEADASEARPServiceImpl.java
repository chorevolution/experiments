package eu.chorevolution.prosumer.seadasearp.business.impl.service;

import java.util.List;
import java.util.ListIterator;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.seadasearp.EcoFriendlyRoutesInformationRequest;
import eu.chorevolution.prosumer.seadasearp.EcoFriendlyRoutesInformationResponse;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesRequest;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.prosumer.seadasearp.Route;
import eu.chorevolution.prosumer.seadasearp.RouteData;
import eu.chorevolution.prosumer.seadasearp.RouteInfo;
import eu.chorevolution.prosumer.seadasearp.RouteSegment;
import eu.chorevolution.prosumer.seadasearp.RouteSegmentInfo;
import eu.chorevolution.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.prosumer.seadasearp.RoutesSuggestion;
import eu.chorevolution.prosumer.seadasearp.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.seadasearp.business.SEADASEARPService;

@Service
public class SEADASEARPServiceImpl implements SEADASEARPService {

	@Override
	public void getEcoRoutes(EcoRoutesRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of EcoRoutesRequest message from senderParticipantName
		*/
	}     

	@Override
	public void setEcoFriendlyRoutesInformation(EcoFriendlyRoutesInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of EcoFriendlyRoutesInformationResponse message from senderParticipantName
		*/
	}     

    @Override
	public RoutesRequest createRoutesRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    		System.out.println("Got request from driver, sending out request asking for routes");
	    	RoutesRequest routesRequest = new RoutesRequest();
	    	EcoRoutesRequest ecoRoutesRequest = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("ecoRoutesRequest");
	    	routesRequest.setOrigin(ecoRoutesRequest.getOrigin());
	    	routesRequest.setDestination(ecoRoutesRequest.getDestination());
	    	return routesRequest;
    }
    
    @Override
	public EcoRoutesResponse createEcoRoutesResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
	    	System.out.println("Got all route information and ecovalues, now create the response");
	    	EcoFriendlyRoutesInformationResponse inMessage = (EcoFriendlyRoutesInformationResponse) choreographyInstanceMessages.getMessage("ecoFriendlyRoutesInformationResponse");
	    	EcoRoutesResponse result = new EcoRoutesResponse();
	    	result.getRoutes().addAll(inMessage.getRoutes());
	    	EcoFriendlyRoutesInformationRequest reqRoutes = (EcoFriendlyRoutesInformationRequest) choreographyInstanceMessages.getMessage("ecoFriendlyRoutesInformationRequest");
	    	ListIterator<RouteInfo> i1 = result.getRoutes().listIterator();
	    	ListIterator<RouteData> i2 = reqRoutes.getRoutes().listIterator();    	
	    	// Trying to add back lost in choreography information (polyline, instruction...)
	    	while (i1.hasNext() && i2.hasNext()) {
				RouteInfo routeItem = i1.next();
				RouteData routeItem2 = i2.next();
				double routeEcovalue = 0;
				
				ListIterator<RouteSegmentInfo> j1 = routeItem.getRouteSegmentInfo().listIterator();
				ListIterator<RouteSegment> j2 = routeItem2.getRouteSegments().listIterator();
				routeItem.setRoutePolyline(routeItem2.getRoutePolyline());
				while (j1.hasNext() && j2.hasNext()) {
					RouteSegmentInfo routeSegmentItem = j1.next();
					RouteSegment routeSegmentItem2 = j2.next();
					routeSegmentItem.getRouteSegment().setPolyline(routeSegmentItem2.getPolyline());
					routeSegmentItem.getRouteSegment().setDistance(routeSegmentItem2.getDistance());
					routeSegmentItem.getRouteSegment().setInstruction(routeSegmentItem2.getInstruction());
					routeSegmentItem.getRouteSegment().setTime(routeSegmentItem2.getTime());
					routeEcovalue += routeSegmentItem.getEcoValue();
				}
				// to be added a field for calculation of eco value for each route
				routeItem.setEco(routeEcovalue);
			}
	    	return result;
    }
    
    @Override
	public EcoFriendlyRoutesInformationRequest createEcoFriendlyRoutesInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
	    	EcoFriendlyRoutesInformationRequest result = new EcoFriendlyRoutesInformationRequest(); 	
	    	System.out.println("Got route suggestions, now request for traffic and eco values");
	    	List<RoutesSuggestion> resultRoutes = choreographyInstanceMessages.getMessages("routesSuggestion");
	    	int routeId = 0;
	    	int providerId = 0; //dont know how to get provider name in a more generic way, thus add this instead
	    	for (RoutesSuggestion routeSuggestion : resultRoutes) {
	    		providerId ++;
	    		for (Route route : routeSuggestion.getRoutes()) {
	    			routeId ++;
	    			RouteData routeData = new RouteData();
	    			routeData.setId(routeId);
	    			routeData.setProvider("provider"+Integer.toString(providerId));
	    			routeData.setDestination(route.getDestination());	
	    			routeData.setOrigin(route.getOrigin());
	    			routeData.setRoutePolyline(route.getRoutePolyline());
	    			routeData.getRouteSegments().addAll(route.getRouteSegments());
	    			result.getRoutes().add(routeData);
			}
		}
	    	return result;
    }
    
	@Override    
    public void receiveRoutesSuggestion(RoutesSuggestion parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (RoutesSuggestion message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    

}
